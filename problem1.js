function problem1(inventory, id) {
    let car;
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id === id) {
            car = inventory[i]
            break
        }
    }
    return car;
}

module.exports = problem1