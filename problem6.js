function problem6(inventory, car1, car2) {
    let cars = []
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_make === car1 || car2 === inventory[i].car_make) {
            cars.push(inventory[i])
        }
    }
    return cars

}

module.exports = problem6


