function problem5(allYears, year) {
    let years = []
    for (let i = 0; i < allYears.length; i++) {
        if (allYears[i] < year) {
            years.push(allYears[i])
        }
    }
    return years

}

module.exports = problem5

