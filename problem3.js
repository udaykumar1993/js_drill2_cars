function problem3(inventory) {
    let carNames = []
    for (let i = 0; i < inventory.length; i++) {
        carNames.push(inventory[i].car_model)
    }
    return sort(carNames)
}

function sort(array) {
    var flag = false;
    while (!flag) {
        flag = true;
        for (var i = 1; i < array.length; i++) {
            if (array[i - 1].localeCompare(array[i]) > 0) {
                flag = false;
                var tmp = array[i - 1];
                array[i - 1] = array[i];
                array[i] = tmp;
            }
        }
    }
    return array;
}


module.exports = problem3

