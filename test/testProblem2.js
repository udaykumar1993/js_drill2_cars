const inventory = require('../cars');
const problem2 = require('../problem2')

const lastCar = inventory.length - 1
let result = problem2(inventory, lastCar)
if (result) {
    console.log(`Last car is a ${result.car_make} ${result.car_model}`)
} else {
    console.log('there are no cars present in inventory')
}