const inventory = require('../cars')
const problem4 = require('../problem4')
const problem5 = require('../problem5')

const year = 2000
let allYears = problem4(inventory)
let result = problem5(allYears, year)


if (result.length > 0) {
    console.log(`success : ${result.length} cars are older than the year ${year}`)
} else {
    console.log(`failed :there is no cars older than the year ${year}`)
}