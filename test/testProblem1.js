const inventory = require('../cars')
const problem1 = require('../problem1')

let id = 33
let result = problem1(inventory, id)
if (result) {
    console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`)
} else {
    console.log("No car found with id: " + id)
}