const inventory = require('../cars')
const problem6 = require('../problem6')

const car1 = "BMW"
const car2 = "Audi"
let cars = problem6(inventory, car1, car2)
if (cars.length > 0) {
    console.log("Success :" + JSON.stringify(cars))
} else {
    console.log(`Failed : there is no ${car1} and ${car2} cars within the inventory`)
}